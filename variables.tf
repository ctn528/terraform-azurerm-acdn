##Public Global variables
variable cod_app {}
variable cod_env {}
variable cod_loc {}
variable correlativo {}
variable corre_infr_base {
  description = "Especifica el correlativo usado para la infraestructura base"
  type        = string
  default     = "01"
}

##Public ACDN module variables
variable acdn_sku {}             #Pricing tier (code)
variable ecdn_origin_hostname {} #A string that determines the hostname of the origin server. This string can be a domain name, Storage Account endpoint, Web App endpoint.

##Private module variables
locals {
  map_cod_env = { ##Mapping for environment
    d = "DESA"
    c = "CERT"
    p = "PROD"
  }
  map_cod_loc = { ##Mapping for location
    eu2 = "eastus2"
    cu1 = "centralus"
  }
  map_acdn_sku = { #Value mapping for pricing tiers
    standard = "Standard_Verizon"
    premium  = "Premium_Verizon"
  }
  cod_svc                            = "acdn"
  cod_resource_group                 = "rsgr"
  correlativo_inicial                = "01"
  location                           = local.map_cod_loc[lower(var.cod_loc)]
  acdn_sku                           = local.map_acdn_sku[lower(var.acdn_sku)]
  ecdn_querystring_caching_behaviour = local.acdn_sku == "standard_verizon" ? "IgnoreQueryString" : "NotSet" #Sets query string caching behavior

  #Name compositions
  resource_name       = lower("${local.cod_svc}${var.cod_loc}${var.cod_app}${var.cod_env}${var.correlativo}")
  resource_group_name = lower("${local.cod_resource_group}${var.cod_loc}${var.cod_app}${var.cod_env}${var.corre_infr_base}")
  ecdn_origin_name    = "${replace(var.ecdn_origin_hostname, ".", "-")}-${lower(replace(local.acdn_sku, "_", "-"))}" #Origin name

  #Origin name

  ##Tags
  tags = {
    operation   = "IBM"
    codApp      = upper(var.cod_app)
    environment = local.map_cod_env[lower(var.cod_env)]
  }
}

##Tags
variable tags {
  default = {
    provisionedFrom = "Acdn-Module"
    provisionedBy   = "IBM-Terraform"
  }
}

##Context
data "azurerm_subscription" "current" {}
## End of private variables
