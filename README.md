[azure]: https://azure.microsoft.com/es-es/overview/what-is-azure/
[paas]: https://azure.microsoft.com/es-es/overview/what-is-paas/
[service]: https://azure.microsoft.com/en-us/services/cdn/
[lbs]: http://ggeneral.bcp.com.pe/sites/gsti/seg/Lineas%20Base%20de%20Seguridad/Microsoft/Azure/PaaS/Linea%20Base%20de%20Seguridad%20-%20Azure%20CDN.pdf

| Name        | Description |
|-------------|-------------|
| Area TI     | Arquitectura Cloud |
| CSP         | [Microsoft Azure][azure] |
| Categoria   | Entrega de Contenido |
| Recurso     | [Azure Content Delivery Network][service] |
| Tipo        | [PaaS][paas] |
| Código      | POAZ-ACDN |
| Mecanismo   | Script de aprovisionamiento automatizado |

<br>

# Azure Content Delivery Network
---

Azure CDN es una solución para la entrega de contenido de ancho de banda alto. Con Azure CDN, puede almacenar en caché objetos estáticos cargados desde Azure Blob Storage, una aplicación web o cualquier servidor web de acceso público, mediante el servidor de punto de presencia (POP) más cercano. Azure CDN también puede acelerar el contenido dinámico, que no puede almacenarse en caché, aprovechando para ello varias optimizaciones de red y de enrutamiento. Azure CDN ofrece diversos proveedores de servicio, el proveedor estandarizado es Verizon.

* [Línea Base de Seguridad][lbs]

El script de aprovisionamiento incluye la configuración de Línea Base de Seguridad (en adelante LBS) del componente.

<br>

# Módulo Terraform
---

### **Descripción**
Este proyecto está diseñado para ser ejecutado como parte de una __Arquitectura de Referencia__ y/o de forma individual.

<br>

### **Requisitos de ejecución**
* **Tools para ejecución del módulo:**
     * azure-cli >= 2.0.65
     * terraform >= v0.12.6
     * git >= 2.7.4

<br>

# Configuración de Variables
---

### **Conceptos Generales**

* **Variables públicas globales**:
	* Declaración: `variable nombre_var {}`
	* Deben ser ingresadas como argumento en la ejecución del __terraform apply__.
	* Normalmente son proveídas por el usuario o propagadas desde una __Arquitectura de Referencia__, por tal motivo, no tienen valor por default.

* **Variables Locales o privadas**:
	* Declaradas dentro del bloque `Private module variables`.
	* **Son de uso interno y no deben ser extendidas y/o modificadas**.

<br>

### **Variables**

| Nombre | Requerido  | Descripción | Valores Permitidos | Default |
| ------ | :--------: |-------------| ------------------ | ------- |
| cod_app |SI| Especifica el código de la aplicación del portafolio de aplicaciones. | El código de aplicación consta sólo de 4 dígitos. | - |
| cod_env |SI| Especifica el ambiente del recurso. | d, c, p | - |
| cod_loc |SI| Especifica la región del recurso. | eu2, cu1 | - |
| correlativo |SI| Especifica el correlativo del recurso. | Desde 01 en adelante | - |
| acdn_sku |SI| Especifica el flavor para el perfil CDN. | standard, premium | - |
| ecdn_origin_hostname |SI| Especifica el nombre de host del servidor de origen. | FQDN | - |

> Sólo es soportado HTTPS (443) para los FQDN.

* Donde:
    * cod_env: 
        * d = Desarrollo
        * c = Certificación
        * p = Producción
    * cod_loc: 
        * eu2 = East Us 2
        * cu1 = Central Us
    * acdn_sku: 
        * standard = Standard Verizon
        * premium = Premium Verizon
    * ecdn_origin_hostname:
        * Ejemplos:
            * www.dominio.com
            * dominio.com

<br>

# Tags
---

La asignación de los "tags" se realiza de forma automática con los siguientes datos:

<br>

| Tag | Descripción |  Valores  |
| ---------| ----------- |-----------|
| **provisionedBy** |  Ejecutor y método de aprovisionamiento  | IBM-Terraform |
| **provisionedFrom**  | Método de despliegue | Acdn-Module |   
| **operation**  | Proveedor de operación  | IBM |
| **codApp**  | Código de aplicación   | Código de 4 dígitos  |
| **environment**  | Ambiente de la infraestructura de referencia  |  **DESA** (Desarrollo) <br> **CERT** (Certificación) <br> **PROD** (Producción) | 

<br>

# Instrucciones de Aprovisionamiento
---

```bash
##Descarga del módulo - modificar el campo <matricula> (sin <>):
> git clone https://<matricula>@bitbucket.lima.bcp.com.pe/scm/nube/azu-acdn-module.git

##Ubicarse en la carpeta del módulo descargado:
> cd azu-acdn-module

##Iniciar terraform:
> terraform init

##Validar terraform:
> terraform validate

##Validar ejecución (se solicitará el ingreso de las variables globales):
> terraform plan

##Ejecución de ejemplo:
> terraform apply -auto-approve \
    -var 'cod_app=nube' \
    -var 'cod_env=d' \
    -var 'cod_loc=eu2' \
    -var 'correlativo=02' \
    -var 'acdn_sku=standard' \
    -var 'ecdn_origin_hostname=staceu2nubefrntd02.z20.web.core.windows.net'
```
<br>

* **Outputs**

    Por default se ha configurado las siguientes salidas para revisar la información del recurso aprovisionado: 
    `applicationCode, environment, subscription, profileName, endPoint, resourceGroup, flavor`. En caso de requerir mayor información o agregar más campos, se debe modificar el archivo **[output.tf](output.tf)**

    <br>

    * Ejemplo de salida:

        ```bash
        Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

        Outputs:

        applicationCode = nube
        endPoint = https://ecdneu2nubed01.azureedge.net
        environment = DESA
        flavor = Standard_Verizon
        profileName = acdneu2nubed02
        resourceGroup = RSGREU2NUBED01
        subscription = Eureka/1a1a1a1a-1a1a-1a1a-1a1a-1a1a1a1a1a1a
        ```

<br>

# Destrucción del Aprovisionamiento
---

En caso de presentar errores durante la implementación o se desee eliminar el recurso aprovisionado mediante el uso del módulo provisto en este repositorio, se tiene el comando `terraform destroy`. Terraform determinará el orden en que los recursos deben ser destruidos respetando las dependencias presentes. El prefijo `"-"` indicará los recursos que se destruirán.

> _**NOTA**: Se debe conservar el archivo `terraform.tfstate` y se debe ingresar las mismas variables con la que fue aprovisionado el componente._

* **Instrucciones**:

    ```bash
    ##Ubicarse en la carpeta del módulo:
    > cd azu-acdn-module

    ##Ejecución de ejemplo:
    > terraform destroy \
        -var 'cod_app=nube' \
        -var 'cod_env=d' \
        -var 'cod_loc=eu2' \
        -var 'correlativo=02' \
        -var 'acdn_sku=standard' \
        -var 'ecdn_origin_hostname=staceu2nubefrntd02.z20.web.core.windows.net'

    ##Luego de la ejecución del comando anterior, se mostrará la cantidad y la información de los recursos a destruir, en caso de estar conforme con la destrucción ingresar "yes".
    ```

<br>

* **Outputs**

    * Ejemplo de salida:

        ```bash
        Destroy complete! Resources: 3 destroyed.
        ```
