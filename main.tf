provider azurerm {
  version = "1.44.0"
}

###Module composition

#CDN Profile
resource "azurerm_cdn_profile" "acdn" {
  name                = local.resource_name
  resource_group_name = local.resource_group_name
  location            = local.location
  sku                 = local.acdn_sku
  tags                = merge(var.tags, local.tags)
}
#CDN Endpoint
module "ecdn" {
  source = "git::https://bitbucket.lima.bcp.com.pe/scm/iaac/azu-ecdn-module.git?ref=provisioning/arq-ref"
  
  cdn_profile_name                   = azurerm_cdn_profile.acdn.name
  correlativo_endpoint               = local.correlativo_inicial
  ecdn_origin_name                   = local.ecdn_origin_name
  ecdn_origin_hostname               = var.ecdn_origin_hostname
  corre_infr_base                    = var.corre_infr_base
  ecdn_querystring_caching_behaviour = local.ecdn_querystring_caching_behaviour
  tags                               = merge(var.tags, local.tags)
}
